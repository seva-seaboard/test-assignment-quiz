const root = document.querySelector("#root");
const { status_cc1, status_cc3, status_cc4, status_cc6 } = {
  status_cc1: "Вы не ввели текст вопроса. Попробуйте добавить вопрос заново.",
  status_cc3:
    "Вы не ввели правильные ответы. Попробуйте добавить вопрос заново.",
  status_cc4:
    "Все вопросы должны иметь хотя бы один выбранный вариант ответа. Проверьте правильность заполнения.",
  status_cc6:
    "Поле может содержать только уникальные значения '1,2,3,4', разделенные запятой. Попробуйте добавить вопрос заново.",
};
const [prompt_mess1, prompt_mess2] = [
  "Введите текст вопроса:",
  "Введите номера правильных ответов через запятую. Нумерация начинается с 1.",
];
const dataObj = {
  questionBlock: { question: "", answers: [], correctAnswers: "" },
  questionArr: [
    {
      question: "Как в JS можно создать неизменяемую переменную.",
      answers: ["const", "let", "var", "int"],
      correctAnswers: "1",
      attempt: "",
    },
    {
      question: "Что из перечисленного можно отнести к типам данных в js.",
      answers: ["null", "Boolean", "object", "BigInt"],
      correctAnswers: "1,2,3,4",
    },
    {
      question: "Выберите ответы, которые являются методами массивов в JS.",
      answers: ["join", "slice", "parseInt", "reverse"],
      correctAnswers: "2,4",
    },
    {
      question: "Какие из нижеприведенных методов возвращают новый массив.",
      answers: ["splice", "reverse", "forEach", "map"],
      correctAnswers: "4",
    },
    {
      question: "Выберите методы, которыми обладает Set в JS.",
      answers: ["set", "add", "has", "length"],
      correctAnswers: "2,3",
    },
  ],
};

//Return initial questions if localStorage is empty
const getQuestionArr = () => {
  const { questionArr } = dataObj;
  const questions =
    JSON.parse(localStorage.getItem("questionList")) || questionArr;
  return questions;
};

// Create DOM Element helper
const createElement = ({ tagName, className = [], attributes = {} }) => {
  const element = document.createElement(tagName);
  if (className.length) {
    const classNames = className.filter((item) => item);
    element.classList.add(...classNames);
  }
  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );
  return element;
};

const createButtons = () => {
  const startQuizBtn = createElement({
    tagName: "button",
    className: ["btn", "btn__start"],
  });
  const manageQuizBtn = createElement({
    tagName: "button",
    className: ["btn", "btn__add"],
  });
  const submitQuizBtn = createElement({
    tagName: "button",
    className: ["btn", "btn__submit"],
  });
  const backToStart = createElement({
    tagName: "button",
    className: ["btn", "btn__toMain"],
  });
  startQuizBtn.innerText = "Начать тест";
  manageQuizBtn.innerText = "Добавить вопрос";
  submitQuizBtn.innerText = "Отправить";
  backToStart.innerText = "На главную";
  return {
    startQuizBtn,
    manageQuizBtn,
    submitQuizBtn,
    backToStart,
  };
};

const createStartPage = () => {
  root.innerHTML = "";
  const { startQuizBtn, manageQuizBtn } = createButtons();
  const heading = createElement({ tagName: "h1", className: ["heading"] });
  const btns = createElement({ tagName: "div", className: ["btns-container"] });
  const desc = createElement({ tagName: "p", className: ["par__description"] });
  heading.innerText = "Quiz";
  desc.innerText =
    "Проходите тест. Создавайте новые вопросы и добавляйте в тест";
  btns.append(startQuizBtn, manageQuizBtn);
  root.append(heading, desc, btns);
};

// Create question/answers block
const createQuizItem = (quizContainer) => {
  const questions = getQuestionArr();
  questions.forEach((item) => {
    const template = `
        <div class = "quiz__item">
          <p class = "question">${item.question}</p>
          ${item.answers
            .map((answer) => {
              return `<input type = "checkbox"><label = "input-Label">${answer}</label><br>`;
            })
            .join("")}
        </div>
    `;
    quizContainer.insertAdjacentHTML("beforeend", template);
  });
};

const createQuizPage = () => {
  const { submitQuizBtn } = createButtons();
  const backToMain = createElement({ tagName: "p", className: ["backToMain"] });
  backToMain.innerText = "На Главную";
  backToMain.addEventListener("click", createStartPage);
  const quizContainer = createElement({ tagName: "div", className: ["quiz"] });
  setTimeout(() => {
    root.innerHTML = "";
    createQuizItem(quizContainer);
    quizContainer.appendChild(submitQuizBtn);
    root.append(backToMain, quizContainer);
  }, 300);
};

const createCorrectAnswers = (obj) => {
  if (!obj) return;
  let newQuestion = { ...obj };
  let answer = prompt(prompt_mess2);
  if (!answer) {
    alert(status_cc3);
    return;
  }
  const reg = /^[1-4](,[1-4])*$/;
  const answerArr = answer.trim().split(",");
  const answerUniqueVal = [...new Set(answerArr)];
  answer = answerArr.sort((a, b) => a - b).join();
  const uniqueTrigger = answerArr.length === answerUniqueVal.length;
  const regTrigger = answer.match(reg);
  if (!regTrigger || !uniqueTrigger) {
    alert(status_cc6);
    return;
  }
  return { ...newQuestion, correctAnswers: answer };
};

const createAnswers = (obj) => {
  let newQuestion = { ...obj };
  const { answers } = newQuestion;
  if (!answers) return;
  let newAnswers = [...answers];
  for (let i = 1; i <= 4; i += 1) {
    const answer = prompt(`Введите текст ${i} варианта ответа`);
    if (!answer) {
      alert(
        `Вы не ввели текст ${i} варианта ответа. Попробуйте добавить вопрос заново.`
      );
      return;
    }
    newAnswers = [...newAnswers, answer];
  }
  newQuestion = { ...newQuestion, answers: newAnswers };
  return newQuestion;
};

const createQuestion = (obj) => {
  let question = prompt(prompt_mess1);
  while (!question) {
    if (!question) {
      alert(status_cc1);
      return;
    }
    question = prompt(prompt_mess1);
  }
  const newQuestion = Object.assign({}, obj, { question });
  return newQuestion;
};

const createModal = ({ questionCount, count, mistakes }) => {
  const { backToStart } = createButtons();
  const layer = createElement({ tagName: "div", className: ["layer"] });
  const status = createElement({ tagName: "h2", className: ["layer__status"] });
  status.innerText =
    mistakes.length > 0
      ? "Вы неправильно ответили на вопросы:"
      : `Ваш результат: ${count} из ${questionCount}. Вы молодец!`;
  const mistakesList = `
    <div class = "mistakes">
      ${mistakes
        .map(
          ({ num, title }) => `<p class = "mistake">${num + 1}. ${title}</p>`
        )
        .join("")}
      <h3 class = "mistakes__subTitle">Ваш результат: ${count} из ${questionCount}</h3>
    </div>
  `;
  return {
    layer,
    status,
    mistakesList,
    backToStart,
  };
};

const showFinalModal = (result) => {
  root.innerHTML = "";
  const { layer, status, mistakesList, backToStart } = createModal(result);
  layer.appendChild(status);
  result.mistakes.length
    ? layer.insertAdjacentHTML("beforeend", mistakesList)
    : layer.classList.add("layer__success");

  layer.appendChild(backToStart);
  document.body.appendChild(layer);
  backToStart.addEventListener("click", () => {
    createStartPage();
    layer.style.display = "none";
  });
};

const calculateResult = (answers) => {
  const questions = getQuestionArr();
  const result = {
    questionCount: null,
    count: 0,
    mistakes: [],
  };
  questions.forEach((item, i) => {
    const correctAnswer = item.correctAnswers === answers[i].join();
    if (correctAnswer) result.count += 1;
    if (!correctAnswer) result.mistakes.push({ num: i, title: item.question });
  });
  result.questionCount = questions.length;
  showFinalModal(result);
};

const checkQuizAnswers = () => {
  const questionBlocks = document.querySelectorAll(".quiz__item");
  const answerAttempts = [];
  questionBlocks.forEach((block) => {
    const checkBoxes = block.querySelectorAll("input[type='checkbox']");
    const blockArr = [];
    checkBoxes.forEach((input, i) => {
      if (input.hasAttribute("checked")) {
        blockArr.push(i + 1);
      }
    });
    answerAttempts.push(blockArr);
  });
  answerAttempts.every((item) => item.length)
    ? calculateResult(answerAttempts)
    : alert(status_cc4);
};

// Pipe functional composition
const pipe =
  (...fns) =>
  (arg) => {
    return fns.reduce((val, fn) => {
      return fn(val);
    }, arg);
  };

const handleClick = (e) => {
  const target = e.target;
  if (target.matches(".btn__start")) {
    createQuizPage();
  }
  if (target.matches(".btn__add")) {
    let { questionBlock, questionArr } = dataObj;
    const doPipe = pipe(createQuestion, createAnswers, createCorrectAnswers);
    const question = doPipe(questionBlock);
    question ? questionArr.push(question) : null;
    localStorage.setItem("questionList", JSON.stringify(questionArr));
  }
  if (target.matches(".btn__submit")) {
    checkQuizAnswers();
  }
};

const handleChange = (e) => {
  const target = e.target;
  if (target.matches("input[type='checkbox']")) {
    target.checked
      ? target.setAttribute("checked", true)
      : target.removeAttribute("checked");
  }
};

document.body.addEventListener("click", (e) => {
  handleClick(e);
  handleChange(e);
});
window.addEventListener("DOMContentLoaded", createStartPage);
